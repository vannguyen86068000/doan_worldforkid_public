﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using WorldForKid.ConnectSocket;
using static UnityEngine.GraphicsBuffer;

public class DataChoose
{
    public string Username;
    public int IndexTile;
    public bool IsLeft;
}

public class Tile : MonoBehaviour
{
    [SerializeField] private SpawnRandomPosition spawnRandomPosition;
    [SerializeField] private ButtonEventDisplay btn;
    [SerializeField] private Button btnLeft;
    [SerializeField] private Button btnRight;
    private List<Item> _items;
    public List<Item> Items => _items;

    public int GetSumScore()
    {
        var score = 0;
        foreach (var item in _items) 
        {
            score += item.Score;
        }
        return score;
    }

    public void Initialized()
    {
        _items = new List<Item>();

        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(OnClickSelect);

        btnLeft.onClick.RemoveAllListeners();
        btnLeft.onClick.AddListener(OnClickLeft);

        btnRight.onClick.RemoveAllListeners();
        btnRight.onClick.AddListener(OnClickRight);
    }

    private int index => GameManager.Instance.GetIndexTile(this);

    private void OnClickRight()
    {
        //EventManager.Notify(EventName.ChooseLeft, false);
        Debug.Log(index + ":" + false);
        SocketCall.CallChooseTile(index, false);
        btn.SetNormal();
    }

    private void OnClickLeft()
    {
        //EventManager.Notify(EventName.ChooseLeft, true);

        SocketCall.CallChooseTile(index, true);
        btn.SetNormal();
    }

    public void AddItem(Item item, bool hasAnimation)
    {
        if (!_items.Contains(item))
        {
            _items.Add(item);
            var pos = SpawnRandomPosition();
            item.MoveToPosition(pos, GameManager.TIME_ITEM_TO_TILE, hasAnimation, null);
        }
    }

    public Item RemoveFistItem()
    {
        if (_items.Count == 0) return null;
        var item = _items[0];
        _items.RemoveAt(0);
        return item;
    }

    private void OnClickSelect()
    {
        btn.interactable = false;
        //PutItemsToCart();
    }
    public void PutItemsToCart()
    {
        EventManager.Notify(EventName.PutItemsInCart, _items);
        EventManager.Notify(EventName.SelectTile, this);
        _items.Clear();
    }

    public Vector3 SpawnRandomPosition()
    {
        return spawnRandomPosition.Get();
    }
    public void TransferItemToOtherTile(Tile other)
    {
        if (_items.Count == 0) return;
        foreach(var item in _items)
            other.AddItem(item, true);
        _items.Clear();
    }
}
