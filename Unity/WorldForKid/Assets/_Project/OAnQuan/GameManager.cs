﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XXX.UI.Popup;
using static SocketCall;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private ListCircular<Tile> tiles;
    [SerializeField] private SamplePool<Item> itemPool;
    [SerializeField] private Tile myCart;
    [SerializeField] private Tile otherCart;
    [SerializeField] private Tile myCollection;
    [SerializeField] private Tile otherCollection;

    [SerializeField]private ListenMessageSocket<SMessageChooseTile> l_SocketChooseItem;


    private Tile _currentTile;
    private Tile _currentCart => _isMyTurn ? myCart : otherCart;
    private Tile _currentCollection => _isMyTurn ? myCollection : otherCollection;

    private bool _isMyTurn = true;


    public const int INIT_COUNT_ITEM_IN_TILE = 5;
    public const float TIME_ITEM_TO_TILE = 0.5f;
    private List<int> _indexQuans = new List<int>()
    {
        0, 6
    };

    private void Awake()
    {
        itemPool.Prepare(10);
        InitializedTiles();

        myCart.Initialized();
        otherCart.Initialized();

        myCollection.Initialized();
        otherCollection.Initialized();
        SpawnItemToTile();

        EventManager.AddEvent(EventName.PutItemsInCart, PutItemsInCart);
        EventManager.AddEvent(EventName.SelectTile, SelectTile);
        EventManager.AddEvent(EventName.ChooseLeft, ChooseLeft);

        l_SocketChooseItem.RegisterEvent(EventName.Socket_ChooseTile, SocketChooseTile);
    }

    private void SocketChooseTile(SMessageChooseTile data)
    {
        _isMyTurn = DataManager.CheckMineByUsername(data.Username);
        var indexTile = GetIndexTile(data.Index, _isMyTurn);
        var isLeft = _isMyTurn ? data.IsLeft : !data.IsLeft;

        _currentTile = tiles.List[indexTile];
        StartCoroutine(IERaiQuan(isLeft));
        
    }

    int GetIndexTile(int index, bool isMine)
    {
        if(isMine) return index;
        return 12 - index;
    }

    private void OnDestroy()
    {
        EventManager.RemoveEvent(EventName.PutItemsInCart, PutItemsInCart);
        EventManager.RemoveEvent(EventName.SelectTile, SelectTile);
        EventManager.RemoveEvent(EventName.ChooseLeft, ChooseLeft);


    }

    private void SelectTile(object data)
    {
        var tile = data as Tile;
        if (!tile) return;
        _currentTile = tile;
        tiles.SetIndex(tile);
    }

    private void ChooseLeft(object data)
    {
        var chooseLeft = (bool)data;

        StartCoroutine(IERaiQuan(chooseLeft));
    }

    private IEnumerator IERaiQuan(bool chooseLeft)
    {
        Tile lastTile = null;

        // bỏ toàn bộ item trong tile được chọn vào giỏ
        _currentTile.PutItemsToCart();
        yield return new WaitForSeconds(TIME_ITEM_TO_TILE);
        while (true)
        {
            var item = _currentCart.RemoveFistItem();
            if (!item)
            {
                // đoạn này là dải từ giỏ vào đồ
                var tileContinute = GetNextTile(chooseLeft);

                // nếu dải hết thì ăn đúp
                if (tileContinute.Items.Count == 0)
                {
                    while (true)
                    {
                        tileContinute = GetNextTile(chooseLeft);
                        tileContinute.TransferItemToOtherTile(_currentCollection);

                        tileContinute = GetNextTile(chooseLeft);
                        if (tileContinute.Items.Count != 0) break;
                        yield return new WaitForSeconds(TIME_ITEM_TO_TILE);
                    }
                    break;
                }

                // lấy và rải tiếp
                if (CheckOQuan(tileContinute)) break;
                tileContinute.PutItemsToCart();
                yield return new WaitForSeconds(2 * TIME_ITEM_TO_TILE);
                continue;
            }
            lastTile = GetNextTile(chooseLeft);
            lastTile.AddItem(item, true);

            yield return new WaitForSeconds(TIME_ITEM_TO_TILE);
        }

        if (CheckEndGame())
        {
            for (int i = 1; i < 6; i++)
            {
                tiles.List[i].TransferItemToOtherTile(myCart);
            }
            for (int i = 7; i < 12; i++)
            {
                tiles.List[i].TransferItemToOtherTile(otherCart);
            }

            if(myCart.GetSumScore() >= otherCart.GetSumScore())
            {
                PopupManager.Instance.ShowPopupWin(null);
            }
            else
            {
                PopupManager.Instance.ShowPopupWin(null);
            }
        }
    }
    private Tile GetNextTile(bool chooseLeft)
    {
        return chooseLeft ? tiles.Pre() : tiles.Next();
    }
    private void ChangeTurn()
    {
        _isMyTurn = !_isMyTurn;
    }


    private void PutItemsInCart(object data)
    {
        var items = data as List<Item>;
        if (items == null) return;

        var cart = _isMyTurn ? myCart : otherCart;
        foreach (var item in items)
        {
            cart.AddItem(item, true);
        }
    }

    private void SpawnItemToTile()
    {
        foreach (var tile in tiles.List)
        {
            for (int i = 0; i < INIT_COUNT_ITEM_IN_TILE; i++)
            {
                var item = itemPool.Get();
                tile.AddItem(item, true);
            }
        }
    }
    private void InitializedTiles()
    {
        foreach (var tile in tiles.List)
        {
            tile.Initialized();
        }

    }
    public int GetIndexTile(Tile tile)
    {
        for(int i = 0; i < tiles.List.Count; i++)
        {
            if (tiles.List[i].Equals(tile))
            {
                return i;
            }
        }
        return -1;
    }
    private bool CheckOQuan(Tile tile)
    {
        var index = GetIndexTile(tile);
        return _indexQuans.Contains(index);
    }
    private bool CheckEndGame()
    {
        foreach(var i in _indexQuans)
        {
            if (tiles.List[i].Items.Count != 0) return false;
        }
        return true;
    }
}
