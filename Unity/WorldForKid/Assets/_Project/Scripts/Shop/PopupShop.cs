﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


namespace Assets._Project.Scripts.Shop
{
    public class PopupShop : XXX.UI.Popup.BasePopup
    {
        public enum TpyeShop
        {
            Shop,
            Inventory
        }
        public static TpyeShop Mode;

        [SerializeField] private EnhancedScroller1 scroller;
        [SerializeField] private TpyeShop type;
        private List<ShopGroupData> _data;
        private const int MaxColum = 3;

        public override void Initialized(object data = null, Action actionClose = null)
        {
            base.Initialized(data, actionClose);
            Mode = type;
            _data = new List<ShopGroupData>();
            scroller.Initialized();
            scroller.Clear();

            APIRequest.GetShop(HandleGetShop);
        }

        private void HandleGetShop(List<AItem> data)
        {
            var tamData = new List<AItem>();
            tamData.AddRange(data);
            if(Mode == TpyeShop.Inventory) 
            {
                data.Clear();
                foreach(var item in tamData)
                {
                    if (item.Count > 0)
                        data.Add(item);
                }
            }
            _data.Clear();
            var groupData = data.ChunkBy(MaxColum);
            foreach (var group in groupData)
            {
                var item = new ShopGroupData();
                item.Items = group;
                scroller.AddData(item);
            }
        }
    }
}
