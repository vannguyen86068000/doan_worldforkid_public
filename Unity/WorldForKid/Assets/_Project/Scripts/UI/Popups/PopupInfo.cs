using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupInfo : XXX.UI.Popup.BasePopup
{
    [SerializeField] private AvatarDisplay avatarDisplay;

    public override void Initialized(object data = null, Action actionClose = null)
    {
        base.Initialized(data, actionClose);
        avatarDisplay.Initialized(true, "");
    }
}
