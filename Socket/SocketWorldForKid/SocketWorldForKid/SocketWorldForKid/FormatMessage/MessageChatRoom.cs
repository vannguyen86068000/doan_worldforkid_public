﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.FormatMessage.GameQuest;
using SocketWorldForKid.Model;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage
{
    internal class MessageChatRoom : BaseMessage
    {
        public class SChatRoom : DataRoom
        {
            public string Sender { get; set; }
            public string Message { get; set; }
            public DateTime TimeSend { get; set; }
        }
        public class CChatRoom : DataRoom
        {
            public string Sender { get; set; }
            public string Message { get; set; }
            public DateTime TimeSend { get; set; }
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CChatRoom>(msg.message);
            var room = server.FindRoom(respone.IdRoom);

            var dataSend = new BaseRespone<SChatRoom>();
            var chatSend = new SChatRoom()
            {
                IdRoom = room.Id,
                Message = respone.Message,
                Sender = respone.Sender,
                TimeSend = respone.TimeSend,
            };
            dataSend.ToSuccess();
            dataSend.Data = chatSend;

            server.SendToAllInRoom(room, Code.CHATROOM, dataSend);
        }
    }
}
