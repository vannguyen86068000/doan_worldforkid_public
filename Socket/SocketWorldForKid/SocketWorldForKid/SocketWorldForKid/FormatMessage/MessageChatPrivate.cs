﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage
{
    internal class MessageChatPrivate : BaseMessage
    {
        public class ChatPrivateDTO
        {
            public string Sender { get; set; }
            public string Message;
            public string Recevier;
            public DateTime TimeSend { get; set; }
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<ChatPrivateDTO>(msg.message);
            var player = server.FindPlayer(respone.Recevier);

            var dataSend = new BaseRespone<ChatPrivateDTO>();
            dataSend.ToSuccess();
            dataSend.Data = respone;
            server.SendToSingleClient(clientSend, Code.CHATPRIVATE, dataSend);
            if (player == null) return;

            server.SendToSingleClient(player, Code.CHATPRIVATE, dataSend);
        }
    }
}
