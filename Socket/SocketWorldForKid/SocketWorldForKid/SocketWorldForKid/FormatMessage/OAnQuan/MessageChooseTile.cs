﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.FormatMessage.GameQuest;
using SocketWorldForKid.Model;
using SocketWorldForKid.Room;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage.OAnQuan
{
    internal class CMessageChooseTile : DataRoom
    {
        public int Index;
        public bool IsLeft;
        public string Username;
    }
    internal class SMessageChooseTile : DataRoom
    {
        public int Index;
        public bool IsLeft;
        public string Username;
    }
    internal class MessageChooseTile : BaseMessage
    {
        public MessageChooseTile() { }
        public MessageChooseTile(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageChooseTile>(msg.message);
            var room = server.FindRoom(respone.IdRoom);
            var dataSend = new BaseRespone<SMessageChooseTile>();
            dataSend.ToSuccess();
            dataSend.Data = new SMessageChooseTile()
            {
                IdRoom = respone.IdRoom,
                Index = respone.Index,
                IsLeft = respone.IsLeft,
                Username = respone.Username
            };

            server.SendToAllInRoom(room, Code.CHOOSETILE, dataSend); ;
        }
    }
}
