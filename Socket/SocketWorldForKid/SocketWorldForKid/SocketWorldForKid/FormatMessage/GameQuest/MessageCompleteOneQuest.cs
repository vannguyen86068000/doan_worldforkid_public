﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.Model;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using static SocketWorldForKid.FormatMessage.GameQuest.MessageFallOneMessage;

namespace SocketWorldForKid.FormatMessage.GameQuest
{
    internal class CMessageCompleteOneQuest : DataRoom
    {
    }
    internal class SMessageCompleteOneQuest 
    {
        public string IdPlayerCompelete;
        public int score;
    }
    internal class MessageCompleteOneQuest : BaseMessage
    {
        public MessageCompleteOneQuest() { }
        public MessageCompleteOneQuest(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageCompleteOneQuest>(msg.message);
            var room = server.FindRoom(respone.IdRoom);
            clientSend.PlayerInfo.score++; 

            var dataSend = new BaseRespone<SMessageCompleteOneQuest>();
            dataSend.ToSuccess();
            dataSend.Data = new SMessageCompleteOneQuest()
            {
                IdPlayerCompelete = clientSend.PlayerInfo.Id,
                score = clientSend.PlayerInfo.score
            };

            server.SendToAllInRoom(room, Code.COMPELETEONEQUEST, dataSend); ;
        }
    }
}
