﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.Game.Piano;
using SocketWorldForKid.Model;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage.GameQuest
{
    internal class CMessageStartGame : DataRoom
    {
    }
    internal class MessageStartGame : BaseMessage
    {
        public MessageStartGame() { }
        public MessageStartGame(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageStartGame>(msg.message);
            var room = server.FindRoom(respone.IdRoom);

            var dataSend = new BaseRespone<SRoomStatus>();
            dataSend.ToSuccess();
            dataSend.Data = room.GetRoomStatus();

            server.SendToAllInRoom(room, Code.STARTGAME, dataSend);

            Thread.Sleep(1000);

            switch (room.Type)
            {
                case ERoomType.Piano:
                    var dataSend1 = new BaseRespone<List<PianoQuest>>();
                    dataSend1.ToSuccess();
                    dataSend1.Data = PianoGame.InitGame(10);
                    server.SendToAllInRoom(room, Code.DATAGAMEPIANO, dataSend1);
                    break;

                case ERoomType.OAnQuan:
                    var dataSend2 = new BaseRespone<int>();
                    dataSend2.ToSuccess();
                    dataSend2.Data = 1;
                    server.SendToAllInRoom(room, Code.STARTGAMEOANQUAN, dataSend2);

                    break;

            }

        }
    }
}
