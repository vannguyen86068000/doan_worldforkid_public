﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.Game.Piano;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage.GameQuest
{
    internal class MessageFallOneMessage : BaseMessage
    {
        public class CMessageFallOneQuest : DataRoom
        {

        }
        public class SMessageFallOneQuest 
        {
            public string IdPlayerFall;
        }
        public MessageFallOneMessage() { }
        public MessageFallOneMessage(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageFallOneQuest>(msg.message);
            var room = server.FindRoom(respone.IdRoom);
            var dataSend = new BaseRespone<SMessageFallOneQuest>();
            dataSend.ToSuccess();
            dataSend.Data = new SMessageFallOneQuest()
            {
                IdPlayerFall = clientSend.PlayerInfo.Id,
            };

            server.SendToAllInRoom(room, Code.FALLONEQUEST, dataSend);
        }
    }
}
