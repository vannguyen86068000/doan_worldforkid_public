﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.ServerSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage.GameQuest
{
    internal class CMessageWinGame : DataRoom
    {
    }
    internal class SMessageWinGame
    {
        public int Score;
        public int Rank;
    }
    internal class MessageWinGame : BaseMessage
    {
        public MessageWinGame() { }
        public MessageWinGame(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageWinGame>(msg.message);
            var room = server.FindRoom(respone.IdRoom);
            server.SendToAllInRoom(room, msg);
        }
    }
}
