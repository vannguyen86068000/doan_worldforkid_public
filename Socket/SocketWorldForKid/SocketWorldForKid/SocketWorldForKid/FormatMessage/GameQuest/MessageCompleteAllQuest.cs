﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage.GameQuest
{
    internal class CMessageCompleteAllQuest : DataRoom
    {
    }
    public class SMessageCompleteAllQuest
    {
        public int rankIndex;
    }
    internal class MessageCompleteAllQuest : BaseMessage
    {
        public MessageCompleteAllQuest() { }
        public MessageCompleteAllQuest(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageCompleteOneQuest>(msg.message);
            var room = server.FindRoom(respone.IdRoom);
            clientSend.PlayerInfo.completed = true;

            if(room.Clients.All(x => x.PlayerInfo.completed))
            {
                int rank0 = 1;
                int rank1 = 1;
                if (room.Clients[0].PlayerInfo.score > room.Clients[1].PlayerInfo.score)
                {
                    rank0 = 1;
                    rank1 = 2;
                }
                if (room.Clients[0].PlayerInfo.score < room.Clients[1].PlayerInfo.score)
                {
                    rank0 = 2;
                    rank1 = 1;
                }


                var dataSend = new BaseRespone<SMessageCompleteAllQuest>();
                dataSend.ToSuccess();
                dataSend.Data = new SMessageCompleteAllQuest()
                {
                    rankIndex = rank0,
                };
                server.SendToSingleClient(room.Clients[0], Code.COMPELETEALLQUEST, dataSend);
                dataSend.Data = new SMessageCompleteAllQuest()
                {
                    rankIndex = rank1,
                };
                server.SendToSingleClient(room.Clients[1], Code.COMPELETEALLQUEST, dataSend);
            }
        }
    }
}
