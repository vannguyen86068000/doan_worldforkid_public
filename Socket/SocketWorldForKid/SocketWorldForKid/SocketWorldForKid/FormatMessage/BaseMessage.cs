﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.ServerSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage
{
    internal class BaseMessage
    {
        public Code code;
        public string message;
        public BaseMessage() { }
        public BaseMessage(Code code, object data)
        {
            this.code = code;
            message = JsonConvert.SerializeObject(data);
        }
        public virtual void Excute(Server server, Client clientSend, BaseMessage msg)
        {

        }

        //public void Serialize(ref DataStreamWriter writer)
        //{
        //    writer.WriteByte((byte)code);
        //    byte[] chatBytes = System.Text.Encoding.UTF8.GetBytes(message);
        //    int lenght = chatBytes.Length;

        //    writer.WriteInt(lenght);
        //    for (int i = 0; i < lenght; i++)
        //    {
        //        writer.WriteByte(chatBytes[i]);
        //    }
        //}
        ////nhân từ server
        //public void Deserialize(DataStreamReader reader)
        //{
        //    int lenght = reader.ReadInt();
        //    byte[] chatBytes = new byte[lenght];
        //    for (int i = 0; i < lenght; i++)
        //    {
        //        chatBytes[i] = reader.ReadByte();
        //    }
        //    message = System.Text.Encoding.UTF8.GetString(chatBytes);
        //}
        //public virtual void ReceivedOnClient() { }
        //public virtual void ReceivedOnServer(NetworkConnection cnn) { }
    }
}
