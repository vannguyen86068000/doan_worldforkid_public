﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.Game.Piano;
using SocketWorldForKid.Model;
using SocketWorldForKid.Room;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;

namespace SocketWorldForKid.FormatMessage.Room
{
    internal class MessageMatchGame : BaseMessage
    {
        public class CMessageMatchGame : DataRoom
        {
        }
        public MessageMatchGame() { }
        public MessageMatchGame(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageMatchGame>(msg.message);

            var room = server.FindRoom(respone.IdRoom);
            room.stateRoom = EStateRoom.Matching;

            BaseRoom roomMatch = null;
            foreach(var r in server.Rooms)
            {
                if(r.stateRoom == EStateRoom.Matching 
                    && r.Id != room.Id
                    && r.Type == room.Type)
                {
                    roomMatch = r;
                }
            }


            if(roomMatch != null)
            {
                roomMatch.stateRoom = EStateRoom.Normal;
                room.stateRoom = EStateRoom.Normal;
                var newRoom = new BaseRoom(2, roomMatch.Type);
                newRoom.AddPlayer(clientSend);
                newRoom.AddPlayer(roomMatch.Clients[0]);
                newRoom.SetRoomOwner(clientSend);
                server.AddRoom(newRoom);

                var dataSend = new BaseRespone<SRoomStatus>();
                dataSend.ToSuccess();
                dataSend.Data = newRoom.GetRoomStatus();

                server.SendToAllInRoom(newRoom, Code.STARTGAME, dataSend);

                Thread.Sleep(1000);

                switch (room.Type)
                {
                    case ERoomType.Piano:
                        var dataSend1 = new BaseRespone<List<PianoQuest>>();
                        dataSend1.ToSuccess();
                        dataSend1.Data = PianoGame.InitGame(10);
                        server.SendToAllInRoom(newRoom, Code.DATAGAMEPIANO, dataSend1);
                        break;
                }
            }




            //var data = new BaseRespone<SRoomStatus>();
            //data.ToSuccess();
            //data.Data = room.GetRoomStatus();
            //server.SendToAllInRoom(room.Id, Code.JOINROOM, data);
        }
    }
}
