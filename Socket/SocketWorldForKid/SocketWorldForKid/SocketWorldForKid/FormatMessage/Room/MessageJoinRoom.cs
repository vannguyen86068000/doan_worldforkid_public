﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.Model;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage.Room
{
    internal class CMessageJoinRoom
    {
        public int RoomId;
    }
    internal class MessageJoinRoom : BaseMessage
    {
        public MessageJoinRoom() { }
        public MessageJoinRoom(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageJoinRoom>(msg.message);
            var room = server.FindRoom(respone.RoomId);
            var isAdded = room.AddPlayer(clientSend);
            var data = new BaseRespone<SRoomStatus>();
            if (isAdded)
            {
                if(room.Clients.Count == 1)
                {
                    room.SetRoomOwner(clientSend);
                }
                data.ToSuccess();
                data.Data = room.GetRoomStatus();
                server.SendToAllInRoom(room.Id, Code.JOINROOM, data);
            }
            else
            {
                data.ToError("Không tham gia được phòng do phòng đã đầy");
                server.SendToSingleClient(clientSend, Code.JOINROOM, data);
            }
        }
    }
}
