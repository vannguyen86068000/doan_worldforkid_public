﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.Model;
using SocketWorldForKid.Room;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage.Room
{
    internal class CMessageCreateRoom
    {
        public ERoomType RoomType;
    }
    internal class MessageCreateRoom : BaseMessage
    {
        public MessageCreateRoom() { }
        public MessageCreateRoom(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);

            var respone = JsonConvert.DeserializeObject<CMessageCreateRoom>(msg.message);

            var room = new BaseRoom(2, respone.RoomType);
            room.AddPlayer(clientSend);
            room.SetRoomOwner(clientSend);
            server.AddRoom(room);
            var data = room.GetRoomStatus();

            var dataSend = new BaseRespone<SRoomStatus>();
            dataSend.ToSuccess();
            dataSend.Data = data;
            server.SendToSingleClient(clientSend, Code.CREATEROOM, dataSend);
        }
    }
}
