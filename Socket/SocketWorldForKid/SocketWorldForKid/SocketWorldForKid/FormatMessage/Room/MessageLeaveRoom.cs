﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.Model;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage.Room
{
    internal class MessageLeaveRoom : BaseMessage
    {
        public class CMessageLeaveRoom : DataRoom
        {
        }
        public MessageLeaveRoom() { }
        public MessageLeaveRoom(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageLeaveRoom>(msg.message);

            var room = server.FindRoom(respone.IdRoom);
            room.RemovePlayer(clientSend);
            var data = new BaseRespone<SRoomStatus>();
            data.ToSuccess();
            data.Data = room.GetRoomStatus();
            server.SendToAllInRoom(room.Id, Code.JOINROOM, data);
        }
    }
}
