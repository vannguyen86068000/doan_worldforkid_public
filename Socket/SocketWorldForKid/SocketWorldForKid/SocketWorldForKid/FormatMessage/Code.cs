﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage
{
    internal enum Code
    {
        None = 0,
        FIRSTCONNECT = 1,
        CHATROOM = 2,
        CHATPRIVATE = 3, 
        CREATEROOM = 4,
        JOINROOM = 5,
        LEAVEROOM = 6,
        RELOADMESSAGEAPI = 7,
        COMPELETEONEQUEST = 8,
        COMPELETEALLQUEST = 9,
        SURRENDER = 10,
        STARTGAME = 11,
        FALLONEQUEST = 12,
        MATCHGAME = 13,
        DATAGAMEPIANO = 14,
        STARTGAMEOANQUAN = 15,
        CHOOSETILE = 16,
    }
}
