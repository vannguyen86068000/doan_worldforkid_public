﻿using Newtonsoft.Json;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.FormatMessage.Room;
using SocketWorldForKid.Model;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.FormatMessage
{
    internal class CMessageFirstConnect
    {
        public Player player;
    }
    internal class SMessageFirstConnect
    {
        public int IdRoomServer;
    }
    internal class MessageFirstConnect : BaseMessage
    {
        public MessageFirstConnect() { }
        public MessageFirstConnect(Code code, object data) : base(code, data)
        {
        }

        public override void Excute(Server server, Client clientSend, BaseMessage msg)
        {
            base.Excute(server, clientSend, msg);
            var respone = JsonConvert.DeserializeObject<CMessageFirstConnect>(msg.message);

            clientSend.PlayerInfo = respone.player;
            var roomServer = server.RoomServer;
            var data = new SMessageFirstConnect()
            {
                IdRoomServer = roomServer.Id
            };
            var dataSend = new BaseRespone<SMessageFirstConnect>();
            dataSend.ToSuccess();
            dataSend.Data = data;
            server.SendToSingleClient(clientSend, Code.FIRSTCONNECT, dataSend);
        }
    }
}
