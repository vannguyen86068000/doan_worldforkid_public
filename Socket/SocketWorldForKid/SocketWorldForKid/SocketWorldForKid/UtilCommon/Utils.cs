﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.UtilCommon
{
    internal class Utils
    {
        public static void ShowLog(string msg)
        {
            var time = DateTime.Now;
            Console.WriteLine(time.ToString() + ": " + msg + "\n");
        }
        public static void ShowWarming(string msg)
        {
            var time = DateTime.Now;
            Console.WriteLine(time.ToString() + ": " + msg + "\n");
        }
        public static void ShowError(string msg)
        {
            var time = DateTime.Now;
            Console.WriteLine(time.ToString() + ": " + msg + "\n");
        }
    }
}
