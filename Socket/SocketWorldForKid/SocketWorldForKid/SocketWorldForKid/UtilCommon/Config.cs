﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.UtilCommon
{
    internal class Config
    {
        public const int MaxPlayerInServer = 100;
        public const int BufferSize = 8096;
    }
}
