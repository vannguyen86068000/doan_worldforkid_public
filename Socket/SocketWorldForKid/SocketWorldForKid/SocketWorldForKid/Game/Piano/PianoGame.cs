﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.Game.Piano
{
    public enum ETypeOperator
    {
        SUMMARTION = 0, MULTIPLICATION = 1, SUBTRACTION = 2
    }
    internal class PianoQuest
    {
        public int a;
        public int b;
        public ETypeOperator t; // type
        public int[] A; // answer
        public int p;// vị trí đúng pos
    }
    internal class PianoGame
    {
        public static List<PianoQuest> InitGame(int count)
        {
            var result = new List<PianoQuest>();
            for(int i = 0; i < count; i++)
            {
                result.Add(SpawnOneQuest());
            }
            return result;
        }
        private static PianoQuest SpawnOneQuest() 
        {
            var result = new PianoQuest(); 
            Random rand = new Random();
            result.t = (ETypeOperator)rand.Next(0, 3);
           
            var kq = 0;
            switch(result.t)
            {
                case ETypeOperator.SUMMARTION:
                    result.a = rand.Next(10, 30);
                    result.b = rand.Next(10, 30);
                    kq = result.a + result.b;
                    break;
                case ETypeOperator.SUBTRACTION:
                    result.a = rand.Next(30, 50);
                    result.b = rand.Next(10, 30);
                    kq = result.a - result.b;
                    break;
                case ETypeOperator.MULTIPLICATION:
                    result.a = rand.Next(1, 10);
                    result.b = rand.Next(1, 10);
                    kq = result.a * result.b;
                    break;
            }
            var suffList = new List<int>() { 0, 1, 2 };
            suffList.Shuffle();
            var kq1 = RandomResult(kq, 20);
            var kq2 = RandomResult(kq, 20);
            result.A = new int[3];
            result.A[suffList[0]] = kq;
            result.A[suffList[1]] = kq1;
            result.A[suffList[2]] = kq2;
            result.p = suffList[0];
            return result;
        }

        private static int RandomResult(int kq, int distance)
        {
            int result = 0;
            var rand = new Random();
            while (true)
            {
                result = rand.Next(kq - distance, kq + distance);
                if(result != kq) break;
            }

            return result;
        }
    }
}
