﻿using SocketWorldForKid.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.Model
{
    [Serializable]
    internal class SRoomStatus
    {
        public int Id;
        public ERoomType Type;
        public List<Player> PlayerList;
        public string IdOwner;
    }
}
