﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.Shared
{
    internal class BaseRespone<T>
    {
        public int Code;
        public string Message;
        public T Data;

        public void ToSuccess()
        {
            Code = 200;
        }
        public void ToError(string msg)
        {
            Code = 400;
            Message = msg;
        }
    }
}
