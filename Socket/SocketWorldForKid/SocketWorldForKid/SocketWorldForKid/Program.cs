﻿using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.UtilCommon;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;


namespace SocketWorldForKid
{
    class Program
    {
        private static readonly object _lock = new object();
        private static readonly string[] _messages = new string[10];
        private static readonly TcpClient[] _clients = new TcpClient[10];
        private static int _clientCount = 0;

        private static Server serverSocket = new Server();

        static void Main(string[] args)
        {
            serverSocket.Start();
            //TcpListener serverSocket = new TcpListener(IPAddress.Any, 8888);
            //serverSocket.Start();
            //Console.WriteLine("Server started");
            //while (true)
            //{
            //    TcpClient clientSocket = serverSocket.AcceptTcpClient();
            //    lock (_lock)
            //    {
            //        Console.WriteLine("Client " + _clientCount + " connected.");
            //        _clients[_clientCount] = clientSocket;
            //        _clientCount++;
            //        Thread t = new Thread(HandleClient);
            //        t.Start(clientSocket);
            //    }
            //}
        }

        public static void HandleClient(object obj)
        {
            TcpClient clientSocket = (TcpClient)obj;
            int clientIndex = Array.IndexOf(_clients, clientSocket);
            while (true)
            {
                NetworkStream stream = clientSocket.GetStream();
                byte[] buffer = new byte[Config.BufferSize];
                int byteCount = stream.Read(buffer, 0, buffer.Length);
                if (byteCount == 0)
                {
                    lock (_lock)
                    {
                        _clients[clientIndex] = null;
                        Console.WriteLine("Client " + clientIndex + " disconnected.");
                        clientSocket.Close();
                    }
                    break;
                }

                string dataReceived = Encoding.ASCII.GetString(buffer, 0, byteCount);
                Console.WriteLine("Received from client " + clientIndex + ": " + dataReceived);

                // Store the message in the message buffer
                lock (_lock)
                {
                    _messages[clientIndex] = dataReceived;
                }

                // Send the message to all connected clients
                for (int i = 0; i < _clientCount; i++)
                {
                    if (_clients[i] != null)
                    {
                        NetworkStream sendStream = _clients[i].GetStream();
                        byte[] sendBuffer = Encoding.ASCII.GetBytes("From client " + clientIndex + ": " + _messages[clientIndex]);
                        sendStream.Write(sendBuffer, 0, sendBuffer.Length);
                        sendStream.Flush();
                    }
                }
            }
        }
    }
}
