﻿using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.FormatMessage;
using SocketWorldForKid.Model;
using SocketWorldForKid.ServerSocket;
using SocketWorldForKid.Shared;
using SocketWorldForKid.UtilCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.Room
{
    public enum EStateRoom
    {
        Normal,
        Matching,
    }
    internal class BaseRoom
    {
        public static List<BaseRoom> All = new List<BaseRoom>();

        public int Id { get; set; }
        public int MaxPlayerInRoom { get; private set; }
        public List<Client> Clients { get; private set; }
        public Client RoomOwner { get; private set; }
        public ERoomType Type { get; private set; }
        public EStateRoom stateRoom { get; set; }
        public BaseRoom(int maxPlayer, ERoomType type)
        {
            MaxPlayerInRoom = maxPlayer;
            Clients = new List<Client>();
            while (true)
            {
                var rand = new Random();
                Id = rand.Next(1000, 9999);
                var currentRoom = All.Where(x => x.Id == Id).FirstOrDefault();
                if (currentRoom == null) break;
            }
            All.Add(this);
            Type = type;
        }
        public bool AddPlayer(Client client)
        {
            if(Clients.Contains(client)) return false;
            if (Clients.Count < MaxPlayerInRoom)
            {
                Clients.Add(client);
                Utils.ShowLog("Player " + client.IpAddress + "vào phòng " + Id);
                return true;
            }
            else
            {
                Utils.ShowLog("Phòng " + Id + "đã đầy");
                return false;
            }
        }
        public void RemovePlayer(Client client)
        {
            Clients.Remove(client);
        }

        public SRoomStatus GetRoomStatus()
        {
            var status = new SRoomStatus();
            status.Id = Id;
            status.Type = Type;
            status.PlayerList = new List<Player>();
            status.IdOwner = RoomOwner.PlayerInfo.Id;
            foreach(var client in Clients) 
            {
                status.PlayerList.Add(client.PlayerInfo);
            }
            return status;
        }
        public void SetRoomOwner(Client client)
        {
            RoomOwner = client;
        }
    }
}
