﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.Room
{
    internal class MathQuest
    {
        public int x;
        public int y;
        public string o;
        public int r1;
        public int r2;
        public int r3;
        public int kq;
    }
    internal class RoomMath : BaseRoom
    {
        public RoomMath(int maxPlayer) : base(maxPlayer, Shared.ERoomType.Piano){}
        public void SpawnQuest(int count)
        {
            var result = new List<MathQuest>();
            for(int i = 0; i < count; i++)
            {
                result.Add(new MathQuest());
            }
        }
    }
}
