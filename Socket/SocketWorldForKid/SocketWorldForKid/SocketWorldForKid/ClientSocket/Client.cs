﻿using SocketWorldForKid.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.ClientSocket
{
    internal class Client
    {
        public TcpClient Socket;
        public bool isConnecting = false;
        public string IpAddress;
        public int Id;
        public Player PlayerInfo;

        public void Close()
        {
            Socket.Close();
        }
    }
}
