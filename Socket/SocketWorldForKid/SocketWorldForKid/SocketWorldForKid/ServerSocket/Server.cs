﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.FormatMessage;
using SocketWorldForKid.UtilCommon;
using SocketWorldForKid.Room;
using Newtonsoft.Json;
using SocketWorldForKid.Shared;

namespace SocketWorldForKid.ServerSocket
{
    internal class Server
    {
        private static readonly object _lock = new object();
        private TcpListener Socket;
        public List<BaseRoom> Rooms;
        public BaseRoom RoomServer;
        //private Client[] clients;
        public void Start()
        {
            Socket = new TcpListener(IPAddress.Any, 8888);
            Rooms = new List<BaseRoom>();
            RoomServer = new BaseRoom(Config.MaxPlayerInServer, ERoomType.None) { Id = 999999};
            Rooms.Add(RoomServer);
            Socket.Start();
            Utils.ShowLog("Server started!");


            while (true)
            {
                TcpClient clientSocket = Socket.AcceptTcpClient();
                lock (_lock)
                {
                    string ipAddress = ((IPEndPoint)clientSocket.Client.RemoteEndPoint).Address.ToString();
                    Utils.ShowLog("Connected client with ip: " + ipAddress);
                    var client = new Client()
                    {
                        Socket = clientSocket,
                        isConnecting = true,
                        IpAddress = ipAddress
                    };
                    RoomServer.AddPlayer(client);
                    Thread t = new Thread(HandleListenClient);
                    t.Start(client);
                }
            }
        }

        private void HandleListenClient(object obj)
        {
            var client = (Client)obj;
            if (client == null) return;
            while (true)
            {
                NetworkStream stream = client.Socket.GetStream();

                var isDisconnect = HandleReceveice(stream, client);
                if (isDisconnect) break;
            }

        }
        private bool HandleReceveice(NetworkStream stream, Client client)
        {
            byte[] buffer = new byte[Config.BufferSize];
            int byteCount = stream.Read(buffer, 0, buffer.Length);
            if (byteCount == 0)
            {
                lock (_lock)
                {
                    Utils.ShowWarming("Client " + client.IpAddress + " disconnected.");
                    client.Close(); 
                    client = null;

                    return true;
                }
            }

            string dataReceived = Encoding.ASCII.GetString(buffer, 0, byteCount);
            Utils.ShowLog("Received from client " + client.IpAddress + ": " + dataReceived);
            BaseMessage message = JsonConvert.DeserializeObject<BaseMessage>(dataReceived);
            
            ExcuteMessage.Excute(this, client, message);

            return false;
        }

        public void SendToSingleClient(Client client, Code code, object data)
        {
            var msg = new BaseMessage(code, data);
            NetworkStream sendStream = client.Socket.GetStream();
            byte[] sendBuffer = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(msg));
            sendStream.Write(sendBuffer, 0, sendBuffer.Length);
            sendStream.Flush();
        }
        public void SendToAllInRoom(int idRoom, Code code, object data)
        {
            var room = Rooms.Where(x => x.Id == idRoom).FirstOrDefault();
            if (room == null)
            {
                Utils.ShowError("Không tìm thấy phòng có id " + idRoom);
                return;
            }
            SendToAllInRoom(room, code, data);
        }
        public void SendToAllInRoom(BaseRoom room, Code code, object data)
        {
            foreach (var client in room.Clients)
            {
                if (client == null) continue;
                SendToSingleClient(client, code, data);
            }
        }
        public void SendToAllClient(Code code, object data)
        {
            SendToAllInRoom(RoomServer, code, data);
        }




        public void SendToSingleClient(Client client, BaseMessage msg)
        {
            NetworkStream sendStream = client.Socket.GetStream();
            byte[] sendBuffer = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(msg));
            sendStream.Write(sendBuffer, 0, sendBuffer.Length);
            sendStream.Flush();
        }
        public void SendToAllInRoom(int idRoom, BaseMessage msg)
        {
            var room = Rooms.Where(x => x.Id == idRoom).FirstOrDefault();
            if (room == null)
            {
                Utils.ShowError("Không tìm thấy phòng có id " + idRoom);
                return;
            }
            SendToAllInRoom(room, msg);
        }
        public void SendToAllInRoom(BaseRoom room, BaseMessage msg)
        {
            foreach (var client in room.Clients)
            {
                if (client == null) continue;
                SendToSingleClient(client, msg);
            }
        }
        public void SendToAllClient(BaseMessage msg)
        {
            SendToAllInRoom(RoomServer, msg);
        }
        public void AddRoom(BaseRoom room)
        {
            Rooms.Add(room);
        }
        public BaseRoom FindRoom(int idRoom)
        {
            return Rooms.FirstOrDefault(x => x.Id == idRoom);
        }
        public Client FindPlayer(string id)
        {
            return RoomServer.Clients.FirstOrDefault(x => x.PlayerInfo.Id.Equals(id));
        }
    }
}
