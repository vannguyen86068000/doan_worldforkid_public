﻿using SocketWorldForKid.ClientSocket;
using SocketWorldForKid.FormatMessage;
using SocketWorldForKid.FormatMessage.GameQuest;
using SocketWorldForKid.FormatMessage.OAnQuan;
using SocketWorldForKid.FormatMessage.Room;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketWorldForKid.ServerSocket
{
    internal class ExcuteMessage
    {
        public static void Excute(Server server, Client client, BaseMessage msg)
        {
            switch (msg.code)
            {
                case Code.FIRSTCONNECT:
                    new MessageFirstConnect().Excute(server, client, msg); break;
                case Code.CHATROOM:
                    new MessageChatRoom().Excute(server, client, msg); break;
                case Code.CHATPRIVATE:
                    new MessageChatPrivate().Excute(server, client, msg); break;
                case Code.CREATEROOM:
                    new MessageCreateRoom().Excute(server, client, msg); break;
                case Code.JOINROOM:
                    new MessageJoinRoom().Excute(server, client, msg); break;
                case Code.STARTGAME:
                    new MessageStartGame().Excute(server, client, msg); break;
                case Code.COMPELETEONEQUEST:
                    new MessageCompleteOneQuest().Excute(server, client, msg); break;
                case Code.COMPELETEALLQUEST:
                    new MessageCompleteAllQuest().Excute(server, client, msg); break;
                case Code.LEAVEROOM:
                    new MessageLeaveRoom().Excute(server, client, msg); break;
                case Code.MATCHGAME:
                    new MessageMatchGame().Excute(server, client, msg); break;
                case Code.CHOOSETILE:
                    new MessageChooseTile().Excute(server, client, msg); break;
            }
        }
    }
}
