﻿using APIWorldForKid.DataShare.Friend;
using APIWorldForKid.DataShare;
using APIWorldForKid.Models;
using Microsoft.AspNetCore.Mvc;

namespace APIWorldForKid.Controllers
{
    public class ChatController : Controller
    {
        public string Add(Guid id, string otherUsername, string msg, string time)
        {
            var respone = new BaseRespone<int>();

            var dateTime = Convert.ToDateTime(time);
            ChatExtension.Add(id, otherUsername, msg, dateTime);

            return respone.ToSuccess(1).ToJson();
        }

        public string GetAll(Guid id, string otherUsername)
        {
            var respone = new BaseRespone<List<AChatPrivate>>();

            var data = ChatExtension.GetChatList(id, otherUsername);

            return respone.ToSuccess(data).ToJson();
        }
    }
}
