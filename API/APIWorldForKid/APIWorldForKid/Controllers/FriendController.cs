﻿using APIWorldForKid.DataShare;
using APIWorldForKid.DataShare.Friend;
using APIWorldForKid.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace APIWorldForKid.Controllers
{
    public class FriendController : Controller
    {
        WFKDbContext db = new WFKDbContext();
        public string GetFriends(Guid id)
        {
            var respone = new BaseRespone<List<AFriendItem>>();
            var data = FriendExtension.GetFriends(id);
            return respone.ToSuccess(data).ToJson();
        }
        public string FindFriends(Guid id, string smallDisplayName)
        {
            var respone = new BaseRespone<List<AFriendItem>>();
            var data = FriendExtension.FindFriends(id, smallDisplayName);
            return respone.ToSuccess(data).ToJson();
        }
        public string Add(Guid id, string otherUserName)
        {
            var respone = new BaseRespone<int>();
            var result = FriendExtension.Add(id, otherUserName);
            if(result) return respone.ToSuccess().ToJson();
            return respone.ToError("Có lỗi trong quá trình thêm").ToJson();
        }
        public string Remove(Guid id, string otherUserName)
        {
            var respone = new BaseRespone<int>();
            var result = FriendExtension.Remove(id, otherUserName);
            if (result) return respone.ToSuccess().ToJson();
            return respone.ToError("Có lỗi trong quá trình xóa").ToJson();
        }
    }
}
