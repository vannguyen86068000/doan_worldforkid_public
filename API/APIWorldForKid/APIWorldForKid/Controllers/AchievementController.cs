﻿using APIWorldForKid.DataShare;
using APIWorldForKid.Models;
using Microsoft.AspNetCore.Mvc;

namespace APIWorldForKid.Controllers
{
    public class AchievementController : Controller
    {
        public string GetAchievements(Guid id)
        {
            var respone = new BaseRespone<List<AAchievement>>();

            var data = AchievementUserExtension.GetAchievements(id);

            return respone.ToSuccess(data).ToJson();
        }
        public string ReceveieAchievement(Guid id, Guid idAchievement)
        {
            var respone = new BaseRespone<List<AItemAchievement>>();

            var data = AchievementExtension.GetItemsAchievement(idAchievement);

            
            foreach (var item in data)
            {
                if(item.Type == TypeItem.Coin)
                {
                    UserExtension.AddCoin(id, item.Count);
                }
                else if(item.Type == TypeItem.Exp)
                {
                    UserExtension.AddExp(id, item.Count);
                }
            }

            AchievementUserExtension.RewardAchievement(id, idAchievement);

            return respone.ToSuccess(data).ToJson();
        }

        public string CompleteOneAchievement(Guid id, EAchievementType type)
        {
            var respone = new BaseRespone<int>();
            var achie = AchievementExtension.Get(type);
            AchievementUserExtension.CompleteOneAchievement(id, achie.Id);
            return respone.ToSuccess(1).ToJson();
        }
    }
}
