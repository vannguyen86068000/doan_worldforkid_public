﻿using APIWorldForKid.DataShare;
using APIWorldForKid.Models;
using APIWorldForKid.Models.Recceive;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace APIWorldForKid.Controllers
{
    public class UserController : Controller
    {
        WFKDbContext db = new WFKDbContext();

        [HttpPost]
        public string Login()
        {
            string json = Request.Form["data"].ToString();
            var respone = new BaseRespone<User>();
            var userClient = JsonConvert.DeserializeObject<CLoginDTO>(json);
            if (userClient == null)
            {
                respone.ToError("Các trường không được để trống");
                return respone.ToJson();
            }

            var user = db.Users.ToList().GetUserByUsername(userClient.username);
            if (user == null)
            {
                respone.ToError("Tên đăng nhập hoặc mật khẩu bị sai");
                return respone.ToJson();
            }
            if (user.Password.Equals(userClient.password))
            {
                respone.ToSuccess();
                respone.Data = user;
                return respone.ToJson();
            }
            else
            {
                respone.ToError("Tên đăng nhập hoặc mật khẩu bị sai");
                return respone.ToJson();
            }
        }
        public string Register()
        {
            string json = Request.Form["data"].ToString();
            var respone = new BaseRespone<User>();
            var userClient = JsonConvert.DeserializeObject<CLoginDTO>(json);
            if (userClient == null)
            {
                respone.ToError("Các trường không được để trống");
                return respone.ToJson();
            }

            var user = db.Users.ToList().GetUserByUsername(userClient.username);
            if (user != null)
            {
                respone.ToError("Tài khoản đăng nhập đã tồn tại!");
                return respone.ToJson();
            }
            else
            {
                var newUser = new User()
                {
                    Id = Guid.NewGuid(),
                    Username = userClient.username,
                    Password = userClient.password,
                    DisplayName = userClient.username,
                    Coin = 1000,
                    IndexAvatar = 0,
                };
                db.Users.Add(newUser);

                ResultGameExtension.CreateNewForUser(newUser.Id);
                InventoryExtension.SetDefaultDataToNewUser(newUser.Id);
                AchievementUserExtension.AddNewAchievemnts(newUser.Id);
                db.SaveChanges();
                respone.ToSuccess();
                respone.Data = null;
                return respone.ToJson();
            }
        }
        public string GetOtherUser(string username)
        {
            var respone = new BaseRespone<User>();
            var user = UserExtension.GetUserByUsername(username);
            if (user == null)
            {
                return respone.ToError("User không tồn tại").ToJson();
            }
            return respone.ToSuccess(user.ForOther()).ToJson();
        }
        private static string GenerateRandomString(int n)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, n).Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public string UseAvatar(Guid id, int indexAvatar)
        {
            var respone = new BaseRespone<User>();
            var user = UserExtension.UseAvatar(id, indexAvatar);
            if (user == null)
            {
                return respone.ToError("User không tồn tại").ToJson();
            }
            return respone.ToSuccess(user).ToJson();

        }
    }
}
