﻿using APIWorldForKid.DataShare;
using APIWorldForKid.Models;
using APIWorldForKid.Models.Send;
using Microsoft.AspNetCore.Mvc;

namespace APIWorldForKid.Controllers
{
    public class ResultGameController : Controller
    {
        WFKDbContext db = new WFKDbContext();
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public string GetResultForUser(Guid id, EGameType gameType)
        {
            var respone = new BaseRespone<ResultGame>();

            var resultGame = db.ResultGames.ToList()
                .FirstOrDefault(x => x.Id.Equals(id) && x.GameType == gameType);
            if (resultGame == null)
            {
                respone.ToError("Không tìm được");
                return respone.ToJson();
            }

            respone.ToSuccess(resultGame);

            return respone.ToJson();
        }
        [HttpGet]
        public string GetResultsRanking(Guid id, EGameType gameType, int skip, int take)
        {
            var respone = new BaseRespone<List<SRankingItem>>();

            var resultGame = new List<SRankingItem>();
            resultGame.Add(ResultGameExtension.GetForUser(id, gameType));
            resultGame.AddRange(ResultGameExtension.GetRanking(gameType, skip, take));

            respone.ToSuccess(resultGame);

            return respone.ToJson();
        }
        public string WinOneGame(Guid id, EGameType gameType)
        {
            var respone = new BaseRespone<int>();
            ResultGameExtension.WinOneGame(id, gameType);
            return respone.ToSuccess(1).ToJson();
        }
        public string LoseOneGame(Guid id, EGameType gameType)
        {
            var respone = new BaseRespone<int>();
            ResultGameExtension.LoseOneGame(id, gameType);
            return respone.ToSuccess(1).ToJson();
        }
    }
}
