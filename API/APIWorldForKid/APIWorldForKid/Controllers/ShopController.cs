﻿using APIWorldForKid.DataShare;
using APIWorldForKid.DataShare.Shop;
using APIWorldForKid.Models;
using Microsoft.AspNetCore.Mvc;

namespace APIWorldForKid.Controllers
{
    public class ShopController : Controller
    {
        public string GetShop(Guid id)
        {
            var respone = new BaseRespone<List<AItem>>();
            var data = ItemCollectExtension.GetAll(id);
            return respone.ToSuccess(data).ToJson();
        }
        public string BuyItem(Guid id, Guid idItem)
        {
            var respone = new BaseRespone<int>();
            var user = UserExtension.GetUserById(id);
            var item = ItemCollectExtension.GetItem(idItem);
            if(item == null)
            {
                return respone.ToError("Item không tồn tại").ToJson();
            }
            if(user.Coin < item.Cost)
            {
                return respone.ToError("Bạn không có đủ tiền").ToJson();
            }
            InventoryExtension.BuyItem(id, idItem);
            return respone.ToSuccess().ToJson();
        }
    }
}
