﻿using APIWorldForKid.DataShare;
using APIWorldForKid.DataShare.Friend;
using APIWorldForKid.Models;
using Microsoft.AspNetCore.Mvc;

namespace APIWorldForKid.Controllers
{
    public class FriendRequestController : Controller
    {
        WFKDbContext db = new WFKDbContext();
        public string Add(Guid id, string otherUserName)
        {
            var respone = new BaseRespone<int>();
            FriendRequestExtension.Add(id, otherUserName);
            return respone.ToSuccess(1).ToJson();
        }
        public string Remove(Guid id, string otherUserName)
        {
            var respone = new BaseRespone<int>();
            FriendRequestExtension.Remove(id, otherUserName);
            return respone.ToSuccess(1).ToJson();
        }
        public string GetAll(Guid id)
        {
            var respone = new BaseRespone<List<AFriendItem>>();
             var friends = FriendRequestExtension.GetAllRequest(id);
            return respone.ToSuccess(friends).ToJson();
        }
    }
}
