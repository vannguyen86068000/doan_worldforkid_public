﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace APIWorldForKid.Migrations
{
    /// <inheritdoc />
    public partial class â : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Achievements",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    type = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MaxCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Achievements", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ItemCollects",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Cost = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemCollects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    sQuestion = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AnswerA = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AnswerB = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AnswerC = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AnswerD = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AnswerRight = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DisplayName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IndexAvatar = table.Column<int>(type: "int", nullable: false),
                    Coin = table.Column<int>(type: "int", nullable: false),
                    Exp = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AchivementItems",
                columns: table => new
                {
                    IdAchievement = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdItem = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Count = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AchivementItems", x => new { x.IdAchievement, x.IdItem });
                    table.ForeignKey(
                        name: "FK_AchivementItems_Achievements_IdAchievement",
                        column: x => x.IdAchievement,
                        principalTable: "Achievements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AchivementItems_ItemCollects_IdItem",
                        column: x => x.IdItem,
                        principalTable: "ItemCollects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AchievementUsers",
                columns: table => new
                {
                    IdUser = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdAchievement = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CurrentCount = table.Column<int>(type: "int", nullable: false),
                    IsRecevied = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AchievementUsers", x => new { x.IdUser, x.IdAchievement });
                    table.ForeignKey(
                        name: "FK_AchievementUsers_Achievements_IdAchievement",
                        column: x => x.IdAchievement,
                        principalTable: "Achievements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AchievementUsers_Users_IdUser",
                        column: x => x.IdUser,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Chats",
                columns: table => new
                {
                    IdSender = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdRecevier = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TimeSend = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsReaded = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chats", x => new { x.IdSender, x.IdRecevier, x.TimeSend });
                    table.ForeignKey(
                        name: "FK_Chats_Users_IdRecevier",
                        column: x => x.IdRecevier,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Chats_Users_IdSender",
                        column: x => x.IdSender,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FriendRequests",
                columns: table => new
                {
                    IdSender = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdReceive = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FriendRequests", x => new { x.IdSender, x.IdReceive });
                    table.ForeignKey(
                        name: "FK_FriendRequests_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Friends",
                columns: table => new
                {
                    Id1 = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Id2 = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Friends", x => new { x.Id1, x.Id2 });
                    table.ForeignKey(
                        name: "FK_Friends_Users_Id1",
                        column: x => x.Id1,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Friends_Users_Id2",
                        column: x => x.Id2,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Inventories",
                columns: table => new
                {
                    IdUser = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdItem = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Count = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventories", x => new { x.IdUser, x.IdItem });
                    table.ForeignKey(
                        name: "FK_Inventories_ItemCollects_IdItem",
                        column: x => x.IdItem,
                        principalTable: "ItemCollects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Inventories_Users_IdUser",
                        column: x => x.IdUser,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ResultGames",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    GameType = table.Column<int>(type: "int", nullable: false),
                    NumberVictory = table.Column<int>(type: "int", nullable: false),
                    NumberDraw = table.Column<int>(type: "int", nullable: false),
                    NumberDefeat = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResultGames", x => new { x.Id, x.GameType });
                    table.ForeignKey(
                        name: "FK_ResultGames_Users_Id",
                        column: x => x.Id,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Achievements",
                columns: new[] { "Id", "Description", "MaxCount", "Name", "type" },
                values: new object[,]
                {
                    { new Guid("9d6f34e2-cfb9-4108-9580-0e617683a4f8"), "Hãy là người chiến thắng tại game Piano", 1, "Đấu trường toán học", 2 },
                    { new Guid("9f90dfe3-bbf1-4816-93cf-8077ed906d74"), "Hãy là người chiến thắng tại game Ô ăn quan", 1, "Chiến thắng Ô ăn quan", 3 }
                });

            migrationBuilder.InsertData(
                table: "ItemCollects",
                columns: new[] { "Id", "Cost", "Description", "Name", "Type" },
                values: new object[,]
                {
                    { new Guid("59c62163-5bb8-411c-8ab2-e763d54eece6"), 100, "Avata bóng tối", "Avata ánh sáng", 2 },
                    { new Guid("683ab348-15d9-4e52-afaa-def16853aa7c"), 100, "Avata bóng tối", "Avata bóng tối", 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AchievementUsers_IdAchievement",
                table: "AchievementUsers",
                column: "IdAchievement");

            migrationBuilder.CreateIndex(
                name: "IX_AchivementItems_IdItem",
                table: "AchivementItems",
                column: "IdItem");

            migrationBuilder.CreateIndex(
                name: "IX_Chats_IdRecevier",
                table: "Chats",
                column: "IdRecevier");

            migrationBuilder.CreateIndex(
                name: "IX_FriendRequests_UserId",
                table: "FriendRequests",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Friends_Id2",
                table: "Friends",
                column: "Id2");

            migrationBuilder.CreateIndex(
                name: "IX_Inventories_IdItem",
                table: "Inventories",
                column: "IdItem");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AchievementUsers");

            migrationBuilder.DropTable(
                name: "AchivementItems");

            migrationBuilder.DropTable(
                name: "Chats");

            migrationBuilder.DropTable(
                name: "FriendRequests");

            migrationBuilder.DropTable(
                name: "Friends");

            migrationBuilder.DropTable(
                name: "Inventories");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "ResultGames");

            migrationBuilder.DropTable(
                name: "Achievements");

            migrationBuilder.DropTable(
                name: "ItemCollects");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
