﻿

using APIWorldForKid.DataShare.Shop;
using System.ComponentModel.DataAnnotations;

namespace APIWorldForKid.Models
{
    public enum TypeItem
    {
        Coin,
        Avatar_1, 
        Avatar_2, 
        Avatar_3, 
        Avatar_4, 
        Avatar_5, 
        Avatar_6, 
        Avatar_7,
        Avatar_8,
        Avatar_9,
        Avatar_10,
        Exp,
    }
    public class ItemCollect
    {
        [Key]
        public Guid Id {  get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Cost { get; set; }
        public TypeItem Type { get; set; }

        public ICollection<Inventory> Inventories { get; set; }
        public ICollection<AchievementItem> AchievementItems { get; set; }
    }
    public static class ItemCollectExtension
    {
        public static List<AItem> GetAll(Guid id)
        {
            var result = new List<AItem>();
            var db = new WFKDbContext();

            foreach (var item in db.ItemCollects)
            {
                var aitem = new AItem();
                aitem.Id = item.Id;
                aitem.Name = item.Name;
                aitem.Description = item.Description;
                aitem.Type = item.Type;

                var inventory = InventoryExtension.GetItem(id, item.Id);
                if (inventory != null) aitem.Count = inventory.Count;

                result.Add(aitem);
            }

            return result;
        }
        
        public static ItemCollect GetItem(Guid id)
        {
            var db = new WFKDbContext();
            return db.ItemCollects
                .FirstOrDefault(x => x.Id.Equals(id));
        }
    }
}
