﻿using APIWorldForKid.DataShare.Friend;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIWorldForKid.Models
{
    public class FriendRequest
    {
        //[ForeignKey("User")]
        public Guid IdSender { get; set; }
        //[ForeignKey("User")]
        public Guid IdReceive { get; set; }
        public User User { get; set; }

    }
    public static class FriendRequestExtension
    {
        public static List<AFriendItem> GetAllRequest(Guid id)
        {
            var result = new List<AFriendItem>();
            var db = new WFKDbContext();
            foreach (var item in db.FriendRequests)
            {
                if (item.IdReceive.Equals(id))
                {
                    var friend = new AFriendItem();
                    var other = UserExtension.GetUserById(item.IdSender);
                    friend.IsFriend = FriendExtension.CheckFriendRelationship(id, other.Username);
                    friend.Username = other.Username;
                    result.Add(friend);
                }
            }
            return result;
        }
        public static bool Add(Guid id, string otherUsername)
        {
            var user = UserExtension.GetUserByUsername(otherUsername);
            var db = new WFKDbContext();
            var friendRequest = new FriendRequest()
            {
                IdSender = id,
                IdReceive = user.Id
            };
            if(!CheckFriendRequestInDb(friendRequest))
                db.FriendRequests.Add(friendRequest);
            db.SaveChanges();
            return true;
        }
        public static bool Remove(Guid id, string otherUsername)
        {
            var user = UserExtension.GetUserByUsername(otherUsername);
            var db = new WFKDbContext();
            var friendRequest = db.FriendRequests
                .FirstOrDefault(x => x.IdSender.Equals(user.Id) && x.IdReceive.Equals(id));
            if (friendRequest == null) return false;
            db.FriendRequests.Remove(friendRequest);
            db.SaveChanges();
            return true;
        }
        private static bool CheckFriendRequestInDb(FriendRequest friendRequest)
        {
            var db = new WFKDbContext();
            if (db.FriendRequests
                .Any(x => x.IdSender.Equals(friendRequest.IdSender) && x.IdReceive.Equals(friendRequest.IdReceive)))
                return true;
            return false;
        }
    }
}
