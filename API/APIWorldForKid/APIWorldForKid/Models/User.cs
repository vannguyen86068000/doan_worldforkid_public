﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIWorldForKid.Models
{
    public class User
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
        public int IndexAvatar { get; set; }
        public int Coin { get; set; }
        public int Exp { get; set; }

        public ICollection<ResultGame> ResultGames { get; set; }
        public ICollection<Inventory> Inventories { get; set; }
        public ICollection<AchievementUser> AchievementUsers { get; set; }
        public ICollection<Friend> Friends { get; set; }
        public ICollection<Chat> Chats { get; set; }
    }
    public enum MinExpLevel 
    {
        Level_1 = 0,
        Level_2 = 100,
        Level_3 = 200,
        Level_4 = 400,
        Level_5 = 800
    }
    public static class UserExtension
    {
        public static User ForOther(this User user)
        {
            // đoạn này sẽ thay đổi User
            var user2 = user;
            user2.Password = "";
            return user2;
        }
        public static bool CheckForLogin(this User user, string password)
        {
            return user.Password.Equals(password);
        }
        public static User ToCreateNew(this User user, string username, string password)
        {
            user.Id = Guid.NewGuid();
            user.Username = username;
            user.Password = password;
            user.DisplayName = username;
            user.IndexAvatar = (int)TypeItem.Avatar_1;
            user.Coin = 0;
            user.Exp = 0;
            return user;
        }
        public static User GetUserByUsername(this List<User> users, string username)
        {
            return users.FirstOrDefault(x => x.Username.Equals(username));
        }
        public static User GetUserByUsername(string username)
        {
            return new WFKDbContext().Users.FirstOrDefault(x => x.Username.Equals(username));
        }
        public static User? UseAvatar(Guid id, int indexAvatar)
        {
            var db = new WFKDbContext();
            var user = db.Users.FirstOrDefault(x => x.Id.Equals(id));
            if(user != null) user.IndexAvatar = indexAvatar;
            db.SaveChanges();
            return user;

        }
        public static User GetUserById(Guid id)
        {
            return new WFKDbContext().Users.FirstOrDefault(x => x.Id.Equals(id));
        }
        public static List<string> FindUser(string smallDisplayName)
        {
            var result = new List<string>();
            var db = new WFKDbContext();
            foreach (var user in db.Users)
            {
                if (user.DisplayName.ToLower().Contains(smallDisplayName.ToLower()))
                {
                    result.Add(user.Username);
                }
            }
            return result;
        }
        public static void AddCoin(Guid id, int coin)
        {
            var db = new WFKDbContext();
            var user = db.Users.FirstOrDefault(x => x.Id.Equals(id));
            user.Coin += coin;
            db.SaveChanges();
        }
        public static void AddExp(Guid id, int exp)
        {
            var db = new WFKDbContext();
            var user = db.Users.FirstOrDefault(x => x.Id.Equals(id));
            user.Exp += exp;
            db.SaveChanges();
        }
    }
}
