﻿using APIWorldForKid.DataShare.Friend;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIWorldForKid.Models
{
    public class Friend
    {
        //[ForeignKey("User1")]
        public Guid Id1 { get; set; }
        //[ForeignKey("User2")]
        public Guid Id2 { get; set; }

        public User User1 { get; set; }
        public User User2 { get; set; }
    }
    public static class FriendExtension
    {
        public static List<AFriendItem> GetFriends(Guid id)
        {
            var friends = new List<AFriendItem>();
            var db = new WFKDbContext();

            foreach(var friend in db.Friends)
            {
                if (friend.Id1.Equals(id))// || friend.Id2.Equals(id))
                {
                    var user = UserExtension.GetUserById(friend.Id2);
                    if (user != null)
                    {
                        friends.Add(new AFriendItem()
                        {
                            IsFriend = true,
                            Username = user.Username
                        });
                        continue;
                    }
                }
                if (friend.Id2.Equals(id))// || friend.Id2.Equals(id))
                {
                    var user = UserExtension.GetUserById(friend.Id1);
                    if (user != null)
                    {
                        friends.Add(new AFriendItem()
                        {
                            IsFriend = true,
                            Username = user.Username
                        });
                        continue;
                    }
                }
            }
            return friends;
        }

        private static bool CheckUserInFriend(Guid id, Friend friend)
        {
            return friend.Id1.Equals(id) || friend.Id2.Equals(id);
        }

        public static bool Remove(Guid idMine, string username)
        {
            var user = UserExtension.GetUserByUsername(username);
            if (user == null) return false;

            var db = new WFKDbContext();
            Friend friendRemove = null;
            foreach (var friend in db.Friends)
            {
                if (CheckUserInFriend(idMine, friend))
                {
                    if (CheckUserInFriend(user.Id, friend))
                    {
                        friendRemove = friend; break;
                    }
                }
            }
            if (friendRemove == null) return false; 
            db.Friends.Remove(friendRemove);
            db.SaveChanges();
            return true;
        }
        public static bool Add(Guid idMine, string username)
        {
            var user = UserExtension.GetUserByUsername(username);
            if (user == null) return false;

            var db = new WFKDbContext();
            var friend = new Friend()
            {
                Id1 = idMine,
                Id2 = user.Id,
            };
            try
            {
                db.Friends.Add(friend);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        
        public static bool CheckFriendRelationship(Guid idMine, string usernameOther)
        {
            var other = UserExtension.GetUserByUsername(usernameOther);
            if(other == null) return false;

            var db = new WFKDbContext();
            foreach(var friend in db.Friends)
            {
                if(CheckUserInFriend(idMine, friend))
                {
                    if(CheckUserInFriend(other.Id, friend))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static List<AFriendItem> FindFriends(Guid id, string smallDisplayName)
        {
            var result = new List<AFriendItem>();
            var usernames = UserExtension.FindUser(smallDisplayName);
            foreach(var  user in usernames)
            {
                var friend = new AFriendItem();
                friend.IsFriend = CheckFriendRelationship(id, user); 
                friend.Username = user;
                result.Add(friend);
            }
            return result;
        }
    }
}
