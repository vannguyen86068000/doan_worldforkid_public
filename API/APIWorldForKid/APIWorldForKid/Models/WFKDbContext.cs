﻿using Microsoft.EntityFrameworkCore;

namespace APIWorldForKid.Models
{
    public class WFKDbContext : DbContext
    {
        public WFKDbContext(DbContextOptions<WFKDbContext> options) : base(options) { }
        public WFKDbContext() : base() { }
        public DbSet<User> Users { get; set; }
        public DbSet<Friend> Friends { get; set; }
        public DbSet<ResultGame> ResultGames { get; set; }
        public DbSet<Achievement> Achievements { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<ItemCollect> ItemCollects { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<AchievementUser> AchievementUsers { get; set; }
        public DbSet<AchievementItem> AchivementItems { get; set; }
        public DbSet<FriendRequest> FriendRequests { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ResultGame>()
                .HasKey(e => new { e.Id, e.GameType });


            modelBuilder.Entity<AchievementUser>()
                .HasKey(e => new { e.IdUser, e.IdAchievement });

            modelBuilder.Entity<AchievementItem>()
                .HasKey(e => new { e.IdAchievement, e.IdItem });

            #region Chat

            modelBuilder.Entity<Chat>()
                .HasKey(e => new { e.IdSender, e.IdRecevier, e.TimeSend });

            modelBuilder.Entity<Chat>()
            .HasOne(f => f.User1)
            .WithMany(u => u.Chats)
            .HasForeignKey(f => f.IdSender)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Chat>()
            .HasOne(f => f.User2)
            .WithMany()
            .HasForeignKey(f => f.IdRecevier)
            .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region  Friend
            modelBuilder.Entity<Friend>()
                .HasKey(e => new { e.Id1, e.Id2 });

            modelBuilder.Entity<Friend>()
            .HasOne(f => f.User1)
            .WithMany(u => u.Friends)
            .HasForeignKey(f => f.Id1)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Friend>()
            .HasOne(f => f.User2)
            .WithMany()
            .HasForeignKey(f => f.Id2)
            .OnDelete(DeleteBehavior.Restrict);
            #endregion

            modelBuilder.Entity<Inventory>()
                .HasKey(e => new { e.IdUser, e.IdItem });

            modelBuilder.Entity<FriendRequest>()
                .HasKey(e => new { e.IdSender, e.IdReceive });



            #region Add Data Item Collect

            modelBuilder.Entity<ItemCollect>().HasData(
            new ItemCollect
            {
                Id = Guid.NewGuid(),
                Name = "Avata bóng tối",
                Cost = 100,
                Description = "Avata bóng tối",
                Type = TypeItem.Avatar_1
            });

            modelBuilder.Entity<ItemCollect>().HasData(
            new ItemCollect
            {
                Id = Guid.NewGuid(),
                Name = "Avata ánh sáng",
                Cost = 100,
                Description = "Avata bóng tối",
                Type = TypeItem.Avatar_2
            });
            #endregion
            #region Add Data Achievement

            modelBuilder.Entity<Achievement>().HasData(
            new Achievement
            {
                Id = Guid.NewGuid(),
                Name = "Đấu trường toán học",
                Description = "Hãy là người chiến thắng tại game Piano",
                MaxCount = 1,
                type = EAchievementType.WinPiano
            });
            modelBuilder.Entity<Achievement>().HasData(
            new Achievement
            {
                Id = Guid.NewGuid(),
                Name = "Chiến thắng Ô ăn quan",
                Description = "Hãy là người chiến thắng tại game Ô ăn quan",
                MaxCount = 1,
                type = EAchievementType.WinOAnQuan
            });
            #endregion


        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-40EB5F5;Database=Test1;Trusted_Connection=True;Encrypt=False;"); // thay YourConnectionString bằng chuỗi kết nối cơ sở dữ liệu của bạn
            //optionsBuilder.UseSqlServer(Configuration.GetConnectionString("MyDbConnection")); // thay YourConnectionString bằng chuỗi kết nối cơ sở dữ liệu của bạn
        }
    }
}
