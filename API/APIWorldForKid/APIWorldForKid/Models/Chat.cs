﻿using APIWorldForKid.DataShare;
using System.Net.WebSockets;

namespace APIWorldForKid.Models
{
    public class Chat
    {
        public Guid IdSender { get; set; }
        public Guid IdRecevier { get; set; }
        public string Message { get; set; }
        public DateTime TimeSend { get; set; }
        public bool IsReaded { get; set; }

        public User User1 { get; set; }
        public User User2 { get; set; }
    }
    public static class ChatExtension
    {
        public static bool CheckChatOfUser(Guid id, Chat chat)
        {
            return chat.IdSender.Equals(id) || chat.IdRecevier.Equals(id);
        }
        public static bool Add(Guid id, string userNameRecevier, string msg, DateTime time)
        {
            var db = new WFKDbContext();
            var userRecevier = UserExtension.GetUserByUsername(userNameRecevier);
            if(userRecevier == null) { return false; }
            var chat = new Chat()
            {
                IdSender = id,
                IdRecevier = userRecevier.Id,
                Message = msg,
                TimeSend = time,
                IsReaded = false
            };
            db.Chats.Add(chat);
            db.SaveChanges();

            return true;
        }

        public static List<AChatPrivate> GetChatList(Guid id, string usernameOther) 
        {
            var chatList = new List<AChatPrivate>();
            var db = new WFKDbContext();
            var other = UserExtension.GetUserByUsername(usernameOther);
            foreach(var chat in db.Chats)
            {
                if(CheckChatOfUser(id, chat) && CheckChatOfUser(other.Id, chat))
                {
                    var item = new AChatPrivate()
                    {
                        UsernameSender = UserExtension.GetUserById(chat.IdSender).Username,
                        Message = chat.Message,
                        TimeSend = chat.TimeSend,
                    };
                    chatList.Add(item);
                }
            }
            chatList.OrderBy(x => x.TimeSend);

            return chatList;
        }
    }
}
