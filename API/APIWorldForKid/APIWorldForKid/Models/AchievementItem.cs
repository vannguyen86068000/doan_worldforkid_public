﻿using System.ComponentModel.DataAnnotations.Schema;

namespace APIWorldForKid.Models
{
    public class AchievementItem
    {
        [ForeignKey("Achievement")]
        public Guid IdAchievement { get; set; }

        [ForeignKey("ItemCollect")]
        public Guid IdItem { get; set; }
        public int Count { get; set; }

        public ItemCollect ItemCollect { get; set; }
        public Achievement Achievement { get; set; }
    }
    public static class AchievementItemExtension
    {
        public static List<AchievementItem> GetAchievementItemsByIdAchievements(List<Guid> ids)
        {
            using (var context = new WFKDbContext()) // replace with your actual DbContext
            {
                var chie = context.AchivementItems.Where(q => ids.Contains(q.IdAchievement)).ToList();
                return chie;
            }
        }
    }
}
