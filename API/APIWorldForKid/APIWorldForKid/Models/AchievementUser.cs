﻿using APIWorldForKid.DataShare;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIWorldForKid.Models
{
    public class AchievementUser
    {
        [ForeignKey("User")]
        public Guid IdUser { get; set; }
        [ForeignKey("Achievement")]
        public Guid IdAchievement { get; set; }
        public int CurrentCount { get; set; }
        public bool IsRecevied { get; set; }

        public User User { get; set; }
        public Achievement Achievement { get; set; }
    }

    public static class AchievementUserExtension
    {
        public static List<AAchievement> GetAchievements(Guid id)
        {
            var result = new List<AAchievement>();
            var db = new WFKDbContext();
            var achievementUsers = db.AchievementUsers
                .Where(x => x.IdUser.Equals(id))
                .ToList();
            foreach (var achievementUser in achievementUsers)
            {
                var achievement = AchievementExtension.Get(achievementUser.IdAchievement);
                if (achievement != null)
                {
                    var item = new AAchievement()
                    {
                        Id = achievementUser.IdAchievement,
                        Name = achievement.Name,
                        Description = achievement.Description,
                        MaxCount = achievement.MaxCount,
                        CurrentCount = achievementUser.CurrentCount,
                        IsRecevied = achievementUser.IsRecevied
                    };
                    result.Add(item);
                }
            }

            return result;
        }


        public static void AddNewAchievemnt(Guid id, EAchievementType type)
        {
            var db = new WFKDbContext();
            var achievement = AchievementExtension.Get(type);
            var achieUser = new AchievementUser()
            {
                IdAchievement = achievement.Id,
                IdUser = id,
                CurrentCount = 0,
                IsRecevied = false,
            };
            db.AchievementUsers.Add(achieUser);
            db.SaveChanges();
        }

        public static void AddNewAchievemnts(Guid id)
        {
            AddNewAchievemnt(id, EAchievementType.WinPiano);
            AddNewAchievemnt(id, EAchievementType.WinOAnQuan);
        }
        public static void RewardAchievement(Guid id, Guid idAchievement)
        {
            var db = new WFKDbContext();
            var achie = db.AchievementUsers.FirstOrDefault(x => x.IdAchievement.Equals(idAchievement) && x.IdUser.Equals(id));
            achie.IsRecevied = true;
            db.SaveChanges();
        }
        public static void CompleteOneAchievement(Guid id, Guid idAchievement)
        {
            var db = new WFKDbContext();
            var achie = db.AchievementUsers.FirstOrDefault(x => x.IdAchievement.Equals(idAchievement) && x.IdUser.Equals(id));
            achie.CurrentCount++;
            db.SaveChanges();
        }
    }
}
