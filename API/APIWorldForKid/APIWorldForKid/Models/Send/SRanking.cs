﻿namespace APIWorldForKid.Models.Send
{
    [System.Serializable]
    public class SRankingItem
    {
        public string Username;
        public int indexRanking;
        public int victory;
        public int defeat;
        public int draw;
    }
}
