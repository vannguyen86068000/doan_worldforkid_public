﻿

using System.ComponentModel.DataAnnotations;

namespace APIWorldForKid.Models
{
    public class Question
    {
        [Key]
        public Guid Id { get; set; }
        public string sQuestion { get; set; }
        public string AnswerA { get; set; }
        public string AnswerB { get; set; }
        public string AnswerC { get; set; }
        public string AnswerD { get; set; }
        public int AnswerRight { get; set; }
    }
    public static class QuestExtension
    {
        public static List<Question> GetQuestionsByIds(List<Guid> ids)
        {
            using (var context = new WFKDbContext()) 
            {
                var questions = context.Questions.Where(q => ids.Contains(q.Id)).ToList();
                return questions;
            }
        }

    }
}
