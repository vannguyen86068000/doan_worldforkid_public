﻿using APIWorldForKid.DataShare;
using APIWorldForKid.Models.Send;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.WebSockets;

namespace APIWorldForKid.Models
{
    public class ResultGame
    {
        [ForeignKey("User")]
        public Guid Id { get; set; }
        public EGameType GameType { get; set; }
        public int NumberVictory { get; set; }
        public int NumberDraw { get; set; }
        public int NumberDefeat { get; set; }

        public User User { get; set; }
    }

    public static class ResultGameExtension
    {
        #region Create new user
        private static void CreateNew(Guid id, EGameType gameType)
        {
            var db = new WFKDbContext();

            var result = new ResultGame
            {
                Id = id,
                GameType = gameType,
                NumberVictory = 0,
                NumberDraw = 0,
                NumberDefeat = 0,
            };
            db.ResultGames.Add(result);
            db.SaveChanges();
        }
        public static void CreateNewForUser(Guid id)
        {
            CreateNew(id, EGameType.Quest);
            CreateNew(id, EGameType.Piano);
            
        }
        #endregion

        #region Ranking
        public static List<SRankingItem> GetRanking(EGameType type, int skip, int take)
        {
            var db = new WFKDbContext();
            var result = new List<SRankingItem>();
            var resultGames = db.ResultGames.ToList()
                .Where(x => x.GameType == type)
                .OrderBy(x => x.NumberVictory)
                .ToList();
            for (int i = 0; i < resultGames.Count; i++)
            {
                var resultGame = resultGames[i];
                var user = UserExtension.GetUserById(resultGame.Id);
                result.Add(new SRankingItem()
                {
                    Username = user.Username,
                    indexRanking = i + 1,
                    victory = resultGame.NumberVictory,
                    defeat = resultGame.NumberDefeat,
                    draw = resultGame.NumberDraw,
                });
            }


            return result;
        }

        public static SRankingItem GetForUser(Guid id, EGameType type)
        {
            var db = new WFKDbContext();
            var user = UserExtension.GetUserById(id);
            var rankings = GetRanking(type, 0, 100);
            int indexRanking = 0;
            for (int i = 0; i<= rankings.Count; i++)
            {
                if (rankings[i].Username.Equals(user.Username))
                {
                    indexRanking = i + 1;break;
                }
            }
            var result = db.ResultGames.FirstOrDefault(x => x.Id.Equals(id));
            if (result == null) return default;
            return new SRankingItem()
            {
                Username = user.Username,
                victory = result.NumberVictory,
                defeat = result.NumberDefeat,
                draw = result.NumberDraw,
                indexRanking = indexRanking
            };
        }
        public static void WinOneGame(Guid id, EGameType type)
        {
            var db = new WFKDbContext();
            var resultGame = db.ResultGames.FirstOrDefault(x => x.Id.Equals(id) && x.GameType == type);
            resultGame.NumberVictory++;
            db.SaveChanges();
        }
        public static void LoseOneGame(Guid id, EGameType type)
        {
            var db = new WFKDbContext();
            var resultGame = db.ResultGames.FirstOrDefault(x => x.Id.Equals(id) && x.GameType == type);
            resultGame.NumberDraw++;
            db.SaveChanges();
        }
        #endregion
    }
}
