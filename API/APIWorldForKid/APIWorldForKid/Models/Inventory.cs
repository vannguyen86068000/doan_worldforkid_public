﻿using System.ComponentModel.DataAnnotations.Schema;

namespace APIWorldForKid.Models
{
    public class Inventory
    {
        [ForeignKey("User")]
        public Guid IdUser { get; set; }
        [ForeignKey("ItemCollect")]
        public Guid IdItem { get; set; }
        public int Count { get; set; }

        public ItemCollect ItemCollect { get; set; }
        public User User { get; set; }
    }
    public static class InventoryExtension
    {
        public static Inventory GetItem(Guid id, Guid idItem)
        {
            var db = new WFKDbContext();
            return db.Inventories.FirstOrDefault(x => x.IdUser.Equals(id) && x.IdItem.Equals(idItem));
        }
        public static void SetDefaultDataToNewUser(Guid id)
        {
            var db = new WFKDbContext();
            foreach(var item in db.ItemCollects)
            {
                var inventory = new Inventory();
                inventory.IdUser = id;
                inventory.IdItem = item.Id;
                inventory.Count = (item.Type == TypeItem.Avatar_1) ? 1 : 0; 
                db.Inventories.Add(inventory);
            }
            db.SaveChanges();       
        }
        public static bool BuyItem(Guid id, Guid idItem)
        {
            var db = new WFKDbContext();

            var item = db.Inventories
                .FirstOrDefault(x => x.IdUser.Equals(id) && x.IdItem.Equals(idItem));
            if (item == null) return false;
            item.Count++;
            db.SaveChanges();
            return true;
        }
    }

}
