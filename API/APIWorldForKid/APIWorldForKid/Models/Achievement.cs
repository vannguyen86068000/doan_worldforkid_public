﻿using APIWorldForKid.DataShare;
using System.ComponentModel.DataAnnotations;

namespace APIWorldForKid.Models
{
    public enum EAchievementType
    {
        None = 0,
        WinQuest = 1,
        WinPiano = 2,
        WinOAnQuan = 3
    }
    public class Achievement
    {
        [Key]
        public Guid Id { get; set; }
        public EAchievementType type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int MaxCount { get; set;}

        public ICollection<AchievementUser> AchievementUsers { get; set; }
        public ICollection<AchievementItem> AchievementItems { get; set; }
    }
    public static class AchievementExtension
    {
        public static Achievement Get(Guid id)
        {
            var db = new WFKDbContext();
            return db.Achievements.FirstOrDefault(x => x.Id.Equals(id));
        }
        public static Achievement Get(EAchievementType type)
        {
            var db = new WFKDbContext();
            return db.Achievements.FirstOrDefault(x => x.type == type);
        }
        public static List<AItemAchievement> GetItemsAchievement(Guid idAchie)
        {
            var db = new WFKDbContext();
            var result = new List<AItemAchievement>();
            foreach(var achieItem in db.AchivementItems.ToList())
            {
                if (achieItem.IdAchievement.Equals(idAchie))
                {
                    var item = ItemCollectExtension.GetItem(achieItem.IdItem);
                    var data = new AItemAchievement()
                    {
                        Type = item.Type,
                        Count = achieItem.Count
                    };
                    result.Add(data);
                }
            }
            return result;
        }
    } 
}
