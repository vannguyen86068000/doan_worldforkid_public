﻿using APIWorldForKid.Models;

namespace APIWorldForKid.DataShare
{
    public class AItemAchievement
    {
        public TypeItem Type { get; set; }
        public int Count;
    }
}
