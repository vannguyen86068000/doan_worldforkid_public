﻿namespace APIWorldForKid.DataShare
{
    public class AChatPrivate
    {
        public string UsernameSender;
        public string Message { get; set; }
        public DateTime TimeSend {  get; set; }
    }
}
