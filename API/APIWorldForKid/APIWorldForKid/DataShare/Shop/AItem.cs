﻿using APIWorldForKid.Models;
using Microsoft.AspNetCore.Mvc;

namespace APIWorldForKid.DataShare.Shop
{
    public class AItem
    {
        public Guid Id;
        public string Name;
        public string Description;
        public int Cost;
        public TypeItem Type;
        public int Count;
    }
}
