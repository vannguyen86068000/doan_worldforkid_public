﻿using Newtonsoft.Json;

namespace APIWorldForKid.DataShare
{
    public class BaseRespone<T>
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }

        public BaseRespone<T> ToError(string msg)
        {
            Code = 400;
            Message = msg;
            return this;
        }
        public BaseRespone<T> ToSuccess()
        {
            Code = 200;
            return this;
        }
        public BaseRespone<T> ToSuccess(T data)
        {
            Code = 200;
            Data = data;
            return this;
        }
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
