﻿namespace APIWorldForKid.DataShare.Friend
{
    public class AFriendItem
    {
        public string Username { get; set; }
        public bool IsFriend { get; set; }
    }
}
