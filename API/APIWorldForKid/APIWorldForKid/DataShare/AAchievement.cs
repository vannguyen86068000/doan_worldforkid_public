﻿namespace APIWorldForKid.DataShare
{
    public class AAchievement
    {
        public Guid Id;
        public string Name;
        public string Description;
        public int CurrentCount;
        public int MaxCount;
        public bool IsRecevied;
    }
}
